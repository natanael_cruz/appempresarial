//
//  Spinner.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 09/01/20.
//  Copyright © 2020 Natanael Cruz Mota. All rights reserved.
//

import Foundation
import UIKit
var loadSpinner: UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView(style: .large)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        loadSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            loadSpinner?.removeFromSuperview()
            loadSpinner = nil
        }
    }
}
