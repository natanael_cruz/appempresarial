//
//  CheckInternet.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 26/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import SystemConfiguration

public class CheckInternet {
    
    class func Connection () -> Bool {
        var zeroaddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0,0,0,0,0,0,0,0))
        zeroaddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroaddress))
        zeroaddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroaddress){
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1){ zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags : SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability! , &flags) == false {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}
