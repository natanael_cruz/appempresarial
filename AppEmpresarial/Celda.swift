//
//  Celda.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 30/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import UIKit

class Celda: UITableViewCell, RegistrableCell {
    
    static var nibName: String{
        return  "custom"
    }
    
    static var reuseId: String{
        return "custom"
    }

    let gradiente: Colors = Colors()
    @IBOutlet weak var lblNumFolio: UILabel!
    @IBOutlet weak var lblCuerpo: UILabel!
    @IBOutlet weak var lblClasifiacion: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
           super.awakeFromNib()
        setGradientBackground()
        
        
           // Initialization code
       }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setGradientBackground() {
        let colorTop = UIColor(red: 205/255.0, green: 205/255.0, blue: 205/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0).cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: self.frame.height)
        
        
        self.layer.insertSublayer(gradientLayer, at:0)
    }

}
