//
//  Util.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 20/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import Foundation
import UIKit
import CryptoKit

class Validation {
    
    func validaPass(psw: String, psw2: String) -> Bool {
        
        return psw == psw2
    }
    
    func validaCampos(name : String, apPat : String, fecha : String, entFed : String, pass : String, pass2 : String) -> Bool{
        
        return name != "" && apPat != "" && fecha != "" && entFed != "" && pass != "" && pass2 != ""
    }
    
    func validaLength (len : Int) -> Bool {
        
        return len >= 8
    }
    
    func validaSiguiente(name : String, apPat : String, entFed : String, edad : Int) -> Bool {
        
       return name.validaString(cadena: name.lowercased()) && apPat.validaString(cadena: name.lowercased()) && entFed.validaString(cadena: name.lowercased()) && edad >= 18
        
    }
    
    func verificaCadenas(name : String, apPat : String, apMat : String, edad : Int, entFed : String ){
        if name.validaString(cadena: name.lowercased()) != true{
            creaAlerta(titulo: "Error", mensaje: "Ingresa un nombre valido. Recuerda no empezar con espacios", mensajeBtn: "ok")
        }
        
        if apPat.validaString(cadena: apPat.lowercased()) != true{
            creaAlerta(titulo: "Error", mensaje: "Ingresa un apellido paterno valido. Recuerda no empezar con espacios", mensajeBtn: "ok")
        }
        
        if apMat != "" && apMat.validaString(cadena: apMat.lowercased()) != true{
            creaAlerta(titulo: "Error", mensaje: "Ingresa un apellido materno valido. Recuerda no empezar con espacios", mensajeBtn: "ok")
        }
        
        if entFed.validaString(cadena: entFed.lowercased()) != true{
            creaAlerta(titulo: "Error", mensaje: "Ingresa una entidad federativa real. Recuerda no empezar con espacios", mensajeBtn: "ok")
        }
        
        if edad < 18 {
            creaAlerta(titulo: "Error de edad", mensaje: "Para registrarte necesitas ser mayor de edad y tener una edad real. Intentaste ingresar \(edad) como edad", mensajeBtn: "ok")
        }
    }
   
    func creaAlerta (titulo:String , mensaje:String, mensajeBtn:String) {
        let alert = UIAlertController (title: titulo , message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: mensajeBtn, style: .default, handler: nil))
        alert.present(alert , animated: true , completion: nil) }
    
}



extension UIButton {

    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
       
        
        
    }
}

extension Notification.Name {
    
    static var loginSuccess : Notification.Name {
        return .init("Login.success")
    }
}

private var maxLengths = [UITextField: Int]()

extension UITextField {

   @IBInspectable var maxLength: Int {

      get {

          guard let length = maxLengths[self]
             else {
                return Int.max
      }
      return length
   }
   set {
      maxLengths[self] = newValue
      addTarget(
         self,
         action: #selector(limitLength),
         for: UIControl.Event.editingChanged
      )
   }
}
    @objc func limitLength(textField: UITextField) {
    guard let prospectiveText = textField.text,
        prospectiveText.count > maxLength
    else {
        return
    }

   let selection = selectedTextRange
   let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
   text = prospectiveText.substring(to: maxCharIndex)
   selectedTextRange = selection
   }
}

extension String {
    
    
    var bool: Bool? {
        switch self.lowercased() {
        case "true", "y" , "yes", "1" :
            return true
            
            
        case "false", "f", "no", "0":
            return false
            
        
        default:
            return nil
        }
    }
    
    var isValidText:Bool {
        let textRegex = "([A-ZÁÉÍÓÚ\\sa-zñáéíóú\\s]*)"
        let textTest = NSPredicate(format: "SELF MATCHES %@", textRegex)
        return textTest.evaluate(with: self)
    }
    
    func cifrar(input:String) -> String{
        let inputData = Data(input.utf8)
        let hashed = SHA256.hash(data: inputData)
        
        return hashed.compactMap { String(format: "%02x", $0) }.joined()
    }
    
    
    func validaString(cadena : String) -> Bool {
        
        let letters = cadena.map { String($0) }
        var arreglo: [String] = []
//        "[a-z0-9áéíóúñ ]"
        if let regex = try? NSRegularExpression(pattern: "[a-z0-9áéíóúñ ]", options: .caseInsensitive)
           {
               let string = self as NSString

               arreglo = regex.matches(in: self, options: [], range: NSRange(location: 0, length: string.length)).map {
                   string.substring(with: $0.range).replacingOccurrences(of: "#", with: "").lowercased()
               }
           }
        return letters == arreglo
    }
    
    func validaNumeroEmpleado(cadena : String) -> Bool {
        
        let letters = cadena.map { String($0) }
        var arreglo: [String] = []
        
        if let regex = try? NSRegularExpression(pattern: "[0-9]", options: .caseInsensitive)
           {
               let string = self as NSString

               arreglo = regex.matches(in: self, options: [], range: NSRange(location: 0, length: string.length)).map {
                   string.substring(with: $0.range).replacingOccurrences(of: "#", with: "").lowercased()
               }
           }
        return letters == arreglo
    }

}

//Funcion para establecer limite de edad en el picker
extension UIDatePicker {
func set18YearValidation() {
    let currentDate: Date = Date()
    var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    calendar.timeZone = TimeZone(identifier: "UTC")!
    var components: DateComponents = DateComponents()
    components.calendar = calendar
    components.year = -18
    let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
    components.year = -150
    let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
    self.minimumDate = minDate
    self.maximumDate = maxDate
} }
