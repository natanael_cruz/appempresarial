
import UIKit

let primaryColor = UIColor(red: 0.02, green: 0.52, blue: 0.03, alpha: 1.0);
let primaryLightColor = UIColor(red: 0.31, green: 0.71, blue: 0.25, alpha: 1.0);
let primaryDarkColor = UIColor(red: 0.00, green: 0.34, blue: 0.00, alpha: 1.0);
let primaryTextColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.0);
let secondaryColor = UIColor(red: 0.11, green: 0.91, blue: 0.71, alpha: 1.0);
let secondaryLightColor = UIColor(red: 0.43, green: 1.00, blue: 0.91, alpha: 1.0);
let secondaryDarkColor = UIColor(red: 0.00, green: 0.71, blue: 0.53, alpha: 1.0);
let secondaryTextColor = UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.0);


class Colors {
    let colorTop = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 204/255.0, alpha: 1.0)
  let colorBottom = UIColor(red: 221/255.0, green: 255/255.0, blue: 153/255.0, alpha: 1.0)

  let gl: CAGradientLayer

  init() {
    gl = CAGradientLayer()
    gl.colors = [ colorTop, colorBottom]
    gl.locations = [ 0.0, 1.0]
    
  }
}
