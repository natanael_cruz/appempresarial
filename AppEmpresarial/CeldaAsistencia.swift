//
//  CeldaAsistencia.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 09/01/20.
//  Copyright © 2020 Natanael Cruz Mota. All rights reserved.
//

import UIKit

class CeldaAsistencia: UITableViewCell {

    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var lblAsistencia: UILabel!
    @IBOutlet weak var lblHora: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewCard.layer.borderColor = UIColor.blue.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
