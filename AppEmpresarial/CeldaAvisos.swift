//
//  CeldaAvisos.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 06/01/20.
//  Copyright © 2020 Natanael Cruz Mota. All rights reserved.
//

import UIKit

protocol Ver {
       func verAviso(aviso : Aviso)
      
   }

class CeldaAvisos: UITableViewCell {
    
   
    var aviso: Aviso?
    var delegate: Ver?
    @IBOutlet weak var txtTitulo: UILabel!
    @IBOutlet weak var txtFecha: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func verAviso(_ sender: Any) {
        guard let avisoAux = aviso else {return}
        delegate?.verAviso(aviso: avisoAux)
    }
    
}
