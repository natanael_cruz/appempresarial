//
//  Services.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 20/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import Foundation
import UIKit
import CryptoKit

class RegistroEmpleadoRequestTO {
    let userdefault = UserDefaults.standard
    let url = "http://132.248.59.72:8080"
//    let url = "http://10.95.71.86:8080"
//    let url = "http://192.168.15.24:8080"
//    let url = "http://rompope.fi-b.unam.mx:8443"
    let path = "/app/api/bdm/app-empresarial/registrarEmpleado"
    let pathLogin = "/app/api/bdm/app-empresarial/login"
    let pathReporte = "/app/api/bdm/app-empresarial/registrarReporte"
    let pathListaDeReportes = "/app/api/bdm/app-empresarial/consultarReportes"
    let pathAsistencia = "/app/api/bdm/app-empresarial/registrarAsistencia"
    
    
    func newEmployee(empleado:Empleado,callback: @escaping (String) -> ()){
//        let cryptoPass = empleado.strPass.cifrar(input: empleado.strPass)
        
        guard let url = URL(string: "\(url)\(path)") else {return}
        print("Service-> \(url)")
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:Any] = ["nombres" : empleado.strName,"apellidoPaterno" : empleado.strApPat, "apellidoMaterno" : empleado.strApMat,"edad" : empleado.intEdad, "fechaDeNacimiento" : empleado.strFecha, "entidadFederativa" : empleado.strEntFed, "password" : empleado.strPass.cifrar(input: empleado.strPass)]
        guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
        request.httpBody = httpBody
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 15.0
        session.configuration.timeoutIntervalForResource = 30.0
        let task = session.dataTask(with: request) { data, response, error in
            guard error == nil  else {
                print("Error : \(String(describing: error))")
                callback("No se pudo conectar con el servidor. Por favor inténtelo más tarde")
                return
            }
            
            guard let content = data else {
                print("No data")
                return
            }
            
            if let datos = String(data: content, encoding: .utf8) {
                print(datos)
            }
            guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                print("No es un json new empleado")
                return
            }
            
            print("json: \(json)")
            guard let _ = json["numeroDeEmpleado"] as? String else {
                print("Error en json")
                return
            }
            
            
            guard let codigo = json["codigoOperacion"] as? Int else { return }
            if codigo == 0{
                DispatchQueue.main.async{
                    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! ViewController
                    controller.idEmp = json["numeroDeEmpleado"] as! String
                    guard let empleadoPersistent = json["numeroDeEmpleado"]  else {return}
                    self.userdefault.set(empleadoPersistent as! String, forKey: "empleado")
                    
            callback("Empleado registrado correctamente, tu número de empleado es: \(json["numeroDeEmpleado"]!)")
                }
            }
            
            if codigo == 1{
                callback("El empleado \(empleado.strName) ya se encuentra registrado")
            }
            
            if codigo == 2 {
                callback("Formato de datos incorrecto. Verifica tus datos e intente nuevamente")
            }
            
            if codigo == -1 {
                callback("Error inesperado, marcar a soporte técnico")
            }
        }

        task.resume()
    }
    
    func Logeo(usr : String , pass : String , callback : @escaping (String) -> () ){
        
        guard let url = URL(string: "\(url)\(pathLogin)") else {return}
        print("Service-> \(url)")
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:String] = ["numeroDeEmpleado" : usr, "password" : pass]
        guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
        request.httpBody = httpBody
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 15.0
        session.configuration.timeoutIntervalForResource = 30.0
        let task = session.dataTask(with: request) { data, response, error in
            guard error == nil  else {
                print("Error : \(String(describing: error))")
                DispatchQueue.main.async{
                    callback("No se pudo conectar con el servidor. Por favor inténtelo más tarde.") }
                return
            }
            
            guard let content = data else {
                print("No data")
                return
            }
            
            if let datos = String(data: content, encoding: .utf8) {
                print(datos)
            }
            guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                print("No es un json new employee")
                return
            }
            
            
            
            guard let codigo = json["codigoOperacion"] as? Int else { return }
            if codigo == 0{
                DispatchQueue.main.async {
                                   
                    callback("\(codigo)")
                               }
            
                
            }
            
            if codigo == 3{
                DispatchQueue.main.async {
                    callback("\(codigo)")
                }
            
            }
        }

        task.resume()
        
        
    }
    
    func serviceReport(reporte:Reporte,callback: @escaping (String) -> ()){
    //        let cryptoPass = empleado.strPass.cifrar(input: empleado.strPass)
            
            guard let url = URL(string: "\(url)\(pathReporte)") else {return}
            print("Service-> \(url)")
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:Any] = ["numeroDeEmpleado" : reporte.strNoEmpleado, "cuerpo" : reporte.strDescripcion, "codigo" : reporte.intTipo ]
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
            request.httpBody = httpBody
            let session = URLSession.shared
            let task = session.dataTask(with: request) { data, response, error in
                guard error == nil  else {
                    print("Error : \(String(describing: error))")
                    callback("No se pudo conectar con el servidor. Por favor inténtelo más tarde.")
                    return
                }
                
                guard let content = data else {
                    print("No data")
                    return
                }
                
                if let datos = String(data: content, encoding: .utf8) {
                    print(datos)
                }
                guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                    print("No es un json new employye")
                    return
                }
                
                print("json: \(json)")
                guard let _ = json["folio"] as? String else {
                    print("Error en json")
                    return
                }
                
                
                guard let codigo = json["codigoOperacion"] as? Int else { return }
                if codigo == 0{
                    callback("\(json["descripcion"]!). \(json["folio"]!) ")
                    
                }
                
                if codigo == -1{
                    callback("\(json["descripcion"]!)")
                }
                
                if codigo == 2 {
                    callback("\(json["descripcion"]!), verifique su reporte e inténtelo de nuevo")
                }
            }

            task.resume()
        }
    
    func registraAsistencia(numEmpleado : String, img: String, callback: @escaping (String) -> ()){
        
        guard let url = URL(string: "\(url)\(pathAsistencia)") else {return}
                print("Service-> \(url)")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:String] = ["numeroDeEmpleado" : numEmpleado, "imagen" : img]
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
                request.httpBody = httpBody
                let session = URLSession.shared
                    session.configuration.timeoutIntervalForRequest = 15.0
                    session.configuration.timeoutIntervalForResource = 30.0
                let task = session.dataTask(with: request) { data, response, error in
                    guard error == nil  else {
                        print("Error : \(String(describing: error))")
                        DispatchQueue.main.async{
                        callback("No se pudo conectar con el servidor, por favor intente mas tarde.")
                        }
                        return
                    }
                    
                    guard let content = data else {
                        print("No data")
                        return
                    }
                    
                    if let datos = String(data: content, encoding: .utf8) {
                        print(datos)
                    }
                    guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                        print("No es un json new employye")
                        return
                    }
                    

                    
                    guard let codigo = json["codigoOperacion"] as? Int else { return }
                    if codigo == 0{
                        DispatchQueue.main.async {
                                           
                            callback("Registro de asistencia exitoso, su folio es: \(String(describing: json["folio"]!))")
                                       }
                    
                        
                    }
                    
                    if codigo == -1{
                        DispatchQueue.main.async {
                            callback("\(String(describing: json["descripcion"]))")
                        }
                    
                    }
                    
                    if codigo == 2{
                        DispatchQueue.main.async {
                            callback("\(json["descripcion"]!)")
                        }
                    }
                    
                    if codigo == 4{
                        DispatchQueue.main.async {
                            callback("\(json["descripcion"]!). Capture otra imagen e inténtelo nuevamente")
                        }
                    }
                    
                    if codigo == 5 {
                        DispatchQueue.main.async {
                            callback("\(json["descripcion"]!)")
                        }
                    }
                    
                }

                task.resume()
                
                
    }
    
    func listaDeReportes (numEmpleado : String, callback: @escaping ([String:Any],String) -> Void){
        
        guard let url = URL(string: "\(url)\(pathListaDeReportes)") else {return}
                print("Service-> \(url)")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:String] = ["numeroDeEmpleado" : numEmpleado]
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
                request.httpBody = httpBody
                let session = URLSession.shared
                    session.configuration.timeoutIntervalForRequest = 15.0
                    session.configuration.timeoutIntervalForResource = 30.0
                let task = session.dataTask(with: request) { data, response, error in
                    guard error == nil  else {
                        print("Error : \(String(describing: error))")
                        callback([:],"Error")
                        return
                    }
                    
                    guard let content = data else {
                        print("No data")
                        return
                    }
                    
                    if let datos = String(data: content, encoding: .utf8) {
                        print(datos)
                    }
                    guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                        print("No es un json new employye")
                        return
                    }

                               
                               
                               
                    
                    guard let codigo = json["codigoOperacion"] as? Int else { return }
                    if codigo == 0{
                        DispatchQueue.main.async {
                            
                            
                                       }
                    
                        
                    }
                    
                    if codigo == -1{
                        DispatchQueue.main.async {
                            
                        }
                    
                    }
                    
                    callback(json, "Exito")
                }

                task.resume()
                
                
    }

    
    func alertaConexion(){
        DispatchQueue.main.async {
            let alert = UIAlertController (title: "Error" , message: "No tienes internet o no has podido acceder al servicio", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            alert.present(alert , animated: true , completion: nil)
        }
    }
    
}
