//
//  RequestConsultaAvisosTO.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 07/01/20.
//  Copyright © 2020 Natanael Cruz Mota. All rights reserved.
//

import Foundation
import UIKit
class ServiceAvisos {
    
//    let url = "http://10.95.71.86:8080"
//    let url = "http://rompope.fi-b.unam.mx:8443"
    let url = "http://132.248.59.72:8080"
    let path = "/app/api/bdm/app-empresarial/consultarAvisos"
    let pathstatus = "/app/api/bdm/app-empresarial/cambiarEstado"
    let pathDelete = "/app/api/bdm/app-empresarial/borrarAviso"
    
    
    func listaDeAvisos (numEmpleado : String, callback: @escaping ([String:Any], String) -> Void){
        
        guard let url = URL(string: "\(url)\(path)") else {return}
                print("Service-> \(url)")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:String] = ["numeroDeEmpleado" : numEmpleado]
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
                request.httpBody = httpBody
                let session = URLSession.shared
                    session.configuration.timeoutIntervalForRequest = 15.0
                    session.configuration.timeoutIntervalForResource = 30.0
                let task = session.dataTask(with: request) { data, response, error in
                    guard error == nil  else {
                        print("Error : \(String(describing: error))")
                        callback([:],"Error")
                        return
                    }
                    
                    guard let content = data else {
                        print("No data")
                        return
                    }
                    
                    if let datos = String(data: content, encoding: .utf8) {
                        print(datos)
                    }
                    guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                        print("Bad JSON")
                        return
                    }

                    
                               
                    
                    guard let codigo = json["codigoOperacion"] as? Int else { return }
                    if codigo == 0{
                        DispatchQueue.main.async {
                            
                            
                                       }
                    
                        
                    }
                    
                    if codigo == -1{
                        DispatchQueue.main.async {
                            
                        }
                    
                    }
                    
                    callback(json, "exito")
                }

                task.resume()
                
                
    }
    
    
    func cambiarEstatus (numEmpleado : String, id: String, callback: @escaping (String) -> Void){
        
        guard let url = URL(string: "\(url)\(pathstatus)") else {return}
                print("Service-> \(url)")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:String] = ["numeroDeEmpleado" : numEmpleado, "id" : id]
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
                request.httpBody = httpBody
                let session = URLSession.shared
                    session.configuration.timeoutIntervalForRequest = 15.0
                    session.configuration.timeoutIntervalForResource = 30.0
                let task = session.dataTask(with: request) { data, response, error in
                    guard error == nil  else {
                        print("Error : \(String(describing: error))")
                        
                        return
                    }
                    
                    guard let content = data else {
                        print("No data")
                        return
                    }
                    
                    if let datos = String(data: content, encoding: .utf8) {
                        print(datos)
                    }
                    guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                        print("Bad JSON")
                        return
                    }

                    
       
                    guard let codigo = json["codigoOperacion"] as? Int else { return }
                    if codigo == 0{
                        
                        print("El aviso ha sido leido")
                        callback("Exito")
                        DispatchQueue.main.async {
                            
                            
                        }
                    
                        
                    }
                    
                    if codigo == -1{
                        DispatchQueue.main.async {
                            
                        }
                    
                    }
                    
                    
                }

                task.resume()
                
                
    }

    func eliminaAviso (numEmpleado : String, id: String, callback: @escaping (String) -> Void){
        
        guard let url = URL(string: "\(url)\(pathDelete)") else {return}
                print("Service-> \(url)")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:String] = ["numeroDeEmpleado" : numEmpleado, "id" : id]
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
                request.httpBody = httpBody
                let session = URLSession.shared
                    session.configuration.timeoutIntervalForRequest = 15.0
                    session.configuration.timeoutIntervalForResource = 30.0
                let task = session.dataTask(with: request) { data, response, error in
                    guard error == nil  else {
                        print("Error : \(String(describing: error))")
                        
                        return
                    }
                    
                    guard let content = data else {
                        print("No data")
                        return
                    }
                    
                    if let datos = String(data: content, encoding: .utf8) {
                        print(datos)
                    }
                    guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                        print("Bad JSON")
                        return
                    }
                    
                               
                               
                    
                    guard let codigo = json["codigoOperacion"] as? Int else { return }
                    if codigo == 0{
                        
                        print("Aviso Eliminado")
                        callback(json["descripcion"] as! String)
                        DispatchQueue.main.async {
                            
                            
                                       }
                    
                        
                    }
                    
                    if codigo == -1{
                        DispatchQueue.main.async {
                          callback("Algo salio mal")
                        }
                    
                    }
                    
                    
                }

                task.resume()
                
                
    }

    
    
}
