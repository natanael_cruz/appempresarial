//
//  RequestEmpleado.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 09/01/20.
//  Copyright © 2020 Natanael Cruz Mota. All rights reserved.
//

import Foundation


class ServiceEmpleado{

//    let url = "http://10.95.71.86:8080"
//    let url = "http://rompope.fi-b.unam.mx:8443"
    let url = "http://132.248.59.72:8080"
    let path = "/app/api/bdm/app-empresarial/informacionEmpleado"
    let pathAsistencia = "/app/api/bdm/app-empresarial/consultarAsistencias"
    let pathRegistrado = "/app/api/bdm/app-empresarial/validarAsistencia"
    
    func getDatos (numEmpleado : String, callback: @escaping (String, Int) -> Void){
        
        guard let url = URL(string: "\(url)\(path)") else {return}
                print("Service-> \(url)")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:String] = ["numeroDeEmpleado" : numEmpleado]
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
                request.httpBody = httpBody
                let session = URLSession.shared
                    session.configuration.timeoutIntervalForRequest = 15.0
                    session.configuration.timeoutIntervalForResource = 30.0
                let task = session.dataTask(with: request) { data, response, error in
                    guard error == nil  else {
                        print("Error : \(String(describing: error))")
                        
                        return
                    }
                    
                    guard let content = data else {
                        print("No data")
                        return
                    }
                    
                    if let datos = String(data: content, encoding: .utf8) {
                        print(datos)
                    }
                    guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                        print("Bad JSON")
                        return
                    }
                    
        //            print("json: \(json)")
        //            guard let _ = json[""] as? String else {
        //                print("Error en json")
        //                return
        //            }
                    
                    
                               
                               
                    
                    guard let codigo = json["codigoOperacion"] as? Int else { return }
                    if codigo == 0{
                        DispatchQueue.main.async {
                            
                            guard let nombre = json["nombres"] else {return}
                            guard let apPat = json["apellidoPaterno"] else {return}
                            guard let cont = json["avisosPendientes"] else {return}
                            callback("\(nombre) \(apPat)", cont as! Int )
                            
                                       }
                    
                        
                    }
                    
                    if codigo == -1{
                        DispatchQueue.main.async {
                            callback("\(String(describing: json["descripcion"]))" , 0)
                        }
                    
                    }
                    
                    
                }

                task.resume()
                
                
    }
    
    func listaDeAsistencias (numEmpleado : String, callback: @escaping ([String:Any]) -> Void){
        
        guard let url = URL(string: "\(url)\(pathAsistencia)") else {return}
                print("Service-> \(url)")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:String] = ["numeroDeEmpleado" : numEmpleado]
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
                request.httpBody = httpBody
                let session = URLSession.shared
                    session.configuration.timeoutIntervalForRequest = 15.0
                    session.configuration.timeoutIntervalForResource = 30.0
                let task = session.dataTask(with: request) { data, response, error in
                    guard error == nil  else {
                        print("Error : \(String(describing: error))")
                        
                        return
                    }
                    
                    guard let content = data else {
                        print("No data")
                        return
                    }
                    
                    if let datos = String(data: content, encoding: .utf8) {
                        print(datos)
                    }
                    guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                        print("Bad JSON")
                        return
                    }
                    
                               
                               
                               
                    
                    guard let codigo = json["codigoOperacion"] as? Int else { return }
                    if codigo == 0{
                        DispatchQueue.main.async {
                            
                            
                                       }
                    
                        
                    }
                    
                    if codigo == -1{
                        DispatchQueue.main.async {
                            
                        }
                    
                    }
                    
                    callback(json)
                }

                task.resume()
                
                
    }
    
    func getValidaAsistencia (numEmpleado : String, callback: @escaping (String, Bool) -> Void){
        
        guard let url = URL(string: "\(url)\(pathRegistrado)") else {return}
                print("Service-> \(url)")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let params:[String:String] = ["numeroDeEmpleado" : numEmpleado]
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
                request.httpBody = httpBody
                let session = URLSession.shared
                    session.configuration.timeoutIntervalForRequest = 15.0
                    session.configuration.timeoutIntervalForResource = 30.0
                let task = session.dataTask(with: request) { data, response, error in
                    guard error == nil  else {
                        print("Error : \(String(describing: error))")
                        
                        return
                    }
                    
                    guard let content = data else {
                        print("No data")
                        return
                    }
                    
                    if let datos = String(data: content, encoding: .utf8) {
                        print(datos)
                    }
                    guard let json = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers)) as? [String : Any] else {
                        print("Bad JSON")
                        return
                    }
                    

                               
                               
                    guard let bandera = json["validacion"] as? Bool else {return}
                    guard let codigo = json["codigoOperacion"] as? Int else { return }
                    if codigo == 0{
                        DispatchQueue.main.async {
                            
                            callback("exito",bandera)
                            
                            
                                       }
                    
                        
                    }
                    
                    if codigo == -1{
                        DispatchQueue.main.async {
                            
                        }
                    
                    }
                    
                    
                }

                task.resume()
                
                
    }
    
}
