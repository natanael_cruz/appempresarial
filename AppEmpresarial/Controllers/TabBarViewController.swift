//
//  TabBarViewController.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 25/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {
    var tabBarItemTWO: UITabBarItem = UITabBarItem()
    static var numEmpSes : String?
    static var registrado : Bool?
    let service = ServiceEmpleado()
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tabBarController?.delegate = self
        guard let numero  = TabBarViewController.numEmpSes else{return}
        service.getValidaAsistencia(numEmpleado: numero, callback: { mensaje, flag  in
            
            TabBarViewController.registrado = flag
            
            
        })
        
        UITabBar.appearance().unselectedItemTintColor = #colorLiteral(red: 0.03496106714, green: 0.1929131746, blue: 0.3297995329, alpha: 1)
        
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
     
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("\(item)")
    }

    // UITabBarControllerDelegate
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        print("||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
