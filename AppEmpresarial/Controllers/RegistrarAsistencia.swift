//
//  RegistrarAsistencia.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 30/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import UIKit

class RegistrarAsistencia: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var btnRegistrar: UIButton!
    
    @IBOutlet weak var imgPlace: UIImageView!
    let service : RegistroEmpleadoRequestTO = RegistroEmpleadoRequestTO()
    let imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        guard let bandera = TabBarViewController.registrado else {return}
        super.viewDidLoad()
//        if TabBarViewController.registrado! {
//        DispatchQueue.main.async{
//        let alert = UIAlertController(title: "Atencion", message: "Para registrar tu asistencia toma foto de tu lugar de trabajo o selecciona una de tu galeria", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
//        self.present(alert, animated: true)
//            } }else{
//            let alert = UIAlertController(title: "Atencion", message: "Ya has registrado tu asistencia de hoy, intenta mañana", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { (action : UIAlertAction) in
//                
//                self.dismiss(animated:true) {
//                     let TabViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabBarViewController
//                     TabViewController.selectedIndex = 0
//                     UIApplication.shared.keyWindow?.rootViewController = TabViewController
//                }
//                
//            }))
//            self.present(alert,animated: true)
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       
        if TabBarViewController.registrado! {
            DispatchQueue.main.async{
                let alert = UIAlertController(title: "Atencion", message: "Ya has registrado tu asistencia de hoy, intenta mañana", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { (action : UIAlertAction) in

                    self.tabBarController?.selectedIndex = 0
                    
                    
                }))
                self.present(alert, animated: true)
                }
        }
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
     
         picker.dismiss(animated: true)

         guard let image = info[.editedImage] as? UIImage else {
             print("No image found")
             return
         }
        
        DispatchQueue.main.async {
            self.imgPlace.image = image
            self.btnRegistrar.isEnabled = true
        }
       
    }
    
    
    func getGalery(){
        let galeria = UIImagePickerController()
        galeria.sourceType = .photoLibrary
        galeria.allowsEditing = true
        galeria.delegate = self
        present(galeria,animated: true)
        
        
    }
    
    func getCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let camara = UIImagePickerController()
            camara.sourceType = .camera
            camara.allowsEditing =  true
            camara.delegate = self
            present(camara,animated: true)
            
        }else{
            self.noCamera()
        }
        
    }
    
    func noCamera(){
        let alert = UIAlertController(title: "Camara no disponible", message: "La camara no puede ser utilizada, intente mas tarde", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        present(alert,animated: true,completion: nil)
    }

   
    @IBAction func choiceImage(_ sender: Any) {
        let alert = UIAlertController(title: "Captura lugar de trabajo", message: "¿De dónde desea tomar la imagen?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camara", style: .default, handler: { (action: UIAlertAction) in
            self.getCamera()
            
        }))
        alert.addAction(UIAlertAction(title: "Galeria", style: .default, handler: {(action: UIAlertAction) in
            self.getGalery()
            
        }))
        self.present(alert,animated: true,completion: nil)
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
    }
    
    
    @IBAction func Registrar(_ sender: UIButton) {
//         self.btnRegistrar.isEnabled = false
        self.showSpinner(onView: self.view)
        let auxfoto = resize(imgPlace.image!)
        let imgcodify = ConvertImageToBase64String(img: auxfoto)
        print(auxfoto.size)
        
        guard let usr = TabBarViewController.numEmpSes else {return}
        print(usr)
        
        
        
        //print("imgcodify -> \(imgcodify)")
        service.registraAsistencia(numEmpleado: usr, img: imgcodify, callback: {mensaje in
            self.removeSpinner()
            print(mensaje)
            self.imgPlace.image = UIImage(systemName: "photo")
            self.btnRegistrar.isEnabled = false
            
            let alert = UIAlertController(title: "", message: mensaje, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler:  { (action : UIAlertAction) in

                self.tabBarController?.selectedIndex = 0
                
            }))
            self.present(alert, animated: true , completion: nil)
            
            
        })
        
        DispatchQueue.main.async {
            
//            self.imgPlace.isHidden = true
            
            
        }
            

    }
    
    func ConvertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }
    
    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image!
    }
    
    
    func resize(_ image:UIImage) -> UIImage {
        
        let ancho:CGFloat = image.size.width
        let alto:CGFloat = image.size.height
        let imgRelacion:CGFloat = ancho/alto
        let maximoancho: CGFloat = 480 //Constante para asegurarnos del tamaño
        let nuevoalto:CGFloat = 640//maximoancho / imgRelacion
        let constanteCompresion:CGFloat = 1.0
        
        let rect:CGRect = CGRect(x: 0, y: 0, width: maximoancho, height: nuevoalto)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData:Data = img.jpegData(compressionQuality: constanteCompresion)!
        UIGraphicsEndImageContext()
        return UIImage(data: imageData)!
        
        
        
        
    }
    
}
