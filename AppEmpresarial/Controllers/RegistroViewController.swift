//
//  RegistroViewController.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 19/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import UIKit
import CryptoKit
import CoreData

class RegistroViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btnBack: UINavigationItem!
    var listaEstados: Array<NSManagedObject> = []
    let service : RegistroEmpleadoRequestTO = RegistroEmpleadoRequestTO()
    let verify = Validation()
    let hoy = Date()
    let calendar = Calendar.current
    let userdefault = UserDefaults.standard
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApPat: UITextField!
    @IBOutlet weak var txtApMat: UITextField!
    @IBOutlet weak var txtFecha: UITextField!
    @IBOutlet weak var txtEntidadFed: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var txtPassConfirm: UITextField!
    @IBOutlet weak var btnRegistrar: UIButton!
    
    @IBOutlet weak var btnSee: UIButton!
    @IBOutlet weak var btnSeeConfirm: UIButton!
    let pickerState = UIPickerView()
    
    
    lazy var datePicker : UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(datePickerChanged(_:)), for: .valueChanged)
        picker.set18YearValidation()
        return picker
    }()
    
   
    
    lazy var dateFormatter : DateFormatter = {
        let formatter = DateFormatter()
        
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()

    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickerState.delegate = self
        self.pickerState.dataSource = self
        recuperaEstados()
        print("\(self.listaEstados.count) Estados Registrados")
        txtFecha.inputView = datePicker
        txtEntidadFed.inputView = pickerState
        hideKeyboard()
            
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        
        
//        Observadores
          NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification: )), name: UIResponder.keyboardWillShowNotification, object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification: )), name: UIResponder.keyboardWillHideNotification, object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification: )), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        

        // Do any additional setup after loading the view.
    }
    
    deinit {
            //Stop listening for keyboard hide/show events
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if CheckInternet.Connection() {
            
        }else{
            alertaInternet(msj: "No tienes conexion")
        }
    }

    @objc func datePickerChanged(_ sender: UIDatePicker){
        
        txtFecha.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    
    @IBAction func registrar(_ sender: Any) {
        guard let apPat = txtApPat.text else {return}
        guard let apMat = txtApMat.text else {return}
        guard let name = txtNombre.text else {return}
        guard let fecha = txtFecha.text else {return}
        guard let entFed = txtEntidadFed.text else {return}
        guard let psw = txtPass.text else {return}
        guard let psw2 = txtPassConfirm.text else {return}
        var edad = 0
        
        //////////////////////////// VALIDACIONES /////////////////////////////
        if name.validaString(cadena: name.lowercased()) != true || name.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            creaAlerta(titulo: "Error", mensaje: "El campo de nombre es obligatorio. No puedes empezar con espacios o ingresar caracteres especiales", mensajeBtn: "Aceptar")
            
        }
        
        if apPat.validaString(cadena: apPat.lowercased()) != true || apPat.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            creaAlerta(titulo: "Error", mensaje: "El campo de apellido Paterno es obligatorio. No puedes empezar con espacios o ingresar caracteres especiales", mensajeBtn: "Aceptar")
        }
        
        if apMat.validaString(cadena: apMat.lowercased()) != true  {
            creaAlerta(titulo: "Error", mensaje: "No es obligatorio el apellido materno, sin embargo, no puede tener caracteres especiales", mensajeBtn: "Aceptar")
        }
        
        if fecha == "" {
            creaAlerta(titulo: "Error", mensaje: "La fecha es obligatoria, por favor insertala", mensajeBtn: "Aceptar")
        }else{
            edad = calculaFecha(fecha: fecha)
            if edad < 18 {
                creaAlerta(titulo: "Error", mensaje: "\(edad) es una edad inválida. Tienes que ser mayor de edad para poder registrate", mensajeBtn: "Aceptar")
            }
            
        }
        
        if entFed.validaString(cadena: entFed.lowercased()) != true || entFed == "" {
            creaAlerta(titulo: "Error", mensaje: "El campo de entidad federativa es obligatorio", mensajeBtn: "Aceptar")
        }
        
        if verify.validaLength(len: psw.count) != true {
            creaAlerta(titulo: "Error", mensaje: "El password debe tener un minimo de 8 caracteres", mensajeBtn: "Aceptar")
            
        }else{
            if verify.validaPass(psw: psw, psw2: psw2) != true {
                creaAlerta(titulo: "Error", mensaje: "Los password deben coincidir", mensajeBtn: "Aceptar")
            }
        }
         
        
        /////////////////////////////////Llamado al servicio//////////////////////////////////////
        print(edad)
        if (CheckInternet.Connection()) {
        if verify.validaCampos(name: name, apPat: apPat, fecha: fecha, entFed: entFed, pass: psw, pass2: psw2) && edad >= 18 {
            
            if verify.validaLength(len: psw.count) && verify.validaPass(psw: psw, psw2: psw2){
                saveEmpleado(name: name, apPat: apPat, apMat: apMat, edad: edad , fecha: fecha, entFed: entFed, pass: psw)
                
                

            }
        }
        }else{
            alertaInternet(msj: "No tienes conexion a internet")
        }

        
    }
    
    @IBAction func actionSee(_ sender: UIButton) {
        if (txtPass.isSecureTextEntry){
            sender.setImage(UIImage(named: "HideIcon"), for: .normal)
            txtPass.isSecureTextEntry = false
            sender.tintColor = UIColor(displayP3Red: 41/255, green: 162/255, blue: 1, alpha: 1)
        }else{
            sender.setImage(UIImage(named: "SeeIcon"), for: .highlighted)
            txtPass.isSecureTextEntry = true
            sender.tintColor = .gray
        }
    }
    
    @IBAction func actionSeeConfirm(_ sender: UIButton) {
        if (txtPassConfirm.isSecureTextEntry){
            sender.setImage(UIImage(named: "HideIcon"), for: .normal)
            txtPassConfirm.isSecureTextEntry = false
            sender.tintColor = UIColor(displayP3Red: 41/255, green: 162/255, blue: 1, alpha: 1)
        }else{
            sender.setImage(UIImage(named: "SeeIcon"), for: .highlighted)
            txtPassConfirm.isSecureTextEntry = true
            sender.tintColor = .gray
        }
    }
    
    
    
  /////////////////////////////CONSUMO DEL SERVICIO//////////////////////////
    func saveEmpleado(name : String, apPat : String, apMat : String, edad : Int, fecha : String, entFed: String, pass : String ){
        let empleado:Empleado = Empleado (dictionary: ["nombres" : name.trimmingCharacters(in: .whitespacesAndNewlines), "apellidoPaterno" : apPat.trimmingCharacters(in: .whitespacesAndNewlines), "apellidoMaterno" : apMat.trimmingCharacters(in: .whitespacesAndNewlines), "edad" : edad, "fechaDeNacimiento" : fecha, "entidadFederativa" : entFed, "password" : pass ])
        print (empleado)
        self.showSpinner(onView: self.view)
        service.newEmployee(empleado: empleado, callback: { mensaje in
            print("message callback -> \(mensaje)")
            self.removeSpinner()
            DispatchQueue.main.async{
//                self.creaAlerta(titulo: "", mensaje:mensaje , mensajeBtn: "Aceptar")
                self.alertTransicion(titulo: "", mensaje: mensaje, mensajeBtn: "Aceptar")
                
            }
        }  )
        
    }

    
    func creaAlerta (titulo:String , mensaje:String, mensajeBtn:String) {
        let alert = UIAlertController (title: titulo , message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: mensajeBtn, style: .default, handler: nil))
        self.present(alert , animated: true , completion: nil)
        
    }
    
    func alertTransicion(titulo:String , mensaje:String, mensajeBtn:String) {
        let alert = UIAlertController (title: titulo , message: mensaje, preferredStyle: .alert)
    
        alert.addAction(UIAlertAction(title: mensajeBtn, style: .default, handler: { UIAlertAction in
            self.goToComplete()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert , animated: true , completion: nil)
    }
    
    func goToComplete() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let controller = story.instantiateViewController(identifier: "principal")
        
        self.present(controller, animated: true, completion: nil)
    }
    
    func calculaFecha(fecha:String) -> Int{
        let auxfecha = dateFormatter.date(from: fecha)
        let periodo = calendar.dateComponents([.year,.month,.day], from: auxfecha!, to: hoy)
        let edad = periodo.year
        return edad!
        
    }
    
    func recuperaEstados() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        let fetchreques = NSFetchRequest<NSManagedObject>(entityName: "Estados")
        let sortDescription = NSSortDescriptor(key: "nombre", ascending: true)
        fetchreques.sortDescriptors = [sortDescription]
        fetchreques.returnsObjectsAsFaults = false
        
        
        do {
            let respuesta = try? managedContext.fetch(fetchreques)
            for estado in respuesta!{
                listaEstados.append(estado)
            }
            
        }catch let error as NSError {
            print(error.userInfo)
        }
    }
    

    
    func alertaInternet (msj : String){
        let alert = UIAlertController(title: "Advertencia", message: msj, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @objc func keyboardWillChange(notification: Notification){
        
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        view.frame.origin.y = -keyboardRect.height + 100
        
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
            
        }else{
            view.frame.origin.y = 0
        }
        
    }
    
}

//Manejor de Picker con Delegados
extension RegistroViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.listaEstados.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.listaEstados[row].value(forKey: "nombre") as! String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtEntidadFed.text = self.listaEstados[row].value(forKey: "nombre") as! String
        
    }
    
    
}
