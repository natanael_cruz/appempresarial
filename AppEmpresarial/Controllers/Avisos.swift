//
//  Avisos.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 06/01/20.
//  Copyright © 2020 Natanael Cruz Mota. All rights reserved.
//

import UIKit

class Avisos: UIViewController {
    
    
    
    let service = ServiceAvisos()
    var lista: [Aviso] = []
    var arraylist: [Dictionary<String,Any>]?
    @IBOutlet weak var tabla: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        lista = []
//       obtieneAvisos()


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        lista = []
//        obtieneAvisos()
//        self.tabla.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if CheckInternet.Connection(){
        lista = []
        obtieneAvisos()
            self.tabla.reloadData() }else{
            
            self.creaAlerta(titulo: "Advertencia", mensaje: "El dispositivo no esta conectado a internet, compruebe su conexión e intente de nuevo", mensajeBtn: "Aceptar")
        }
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

//        self.tabla.reloadData()
    }
    
    func obtieneAvisos(){
        self.showSpinner(onView: self.view)
    guard let numEmp = TabBarViewController.numEmpSes else {return}
        self.service.listaDeAvisos(numEmpleado: numEmp, callback: { json, mensaje in
            
            if mensaje == "Error" {
                DispatchQueue.main.async{
                    self.creaAlerta(titulo: "Advertencia", mensaje: "No se pudo conectar con el servidor, por favor intente mas tarde", mensajeBtn: "Aceptar")}
            }else {
        print(json)
            
            self.arraylist = (json["avisos"] as? [Dictionary<String,Any>])
            
            
            for aviso in self.arraylist! {
            let a = self.guardaAvisos(id: aviso["identificador"] as? String ?? "", title: aviso["titulo"] as! String, body: aviso["cuerpo"] as! String, status: aviso["estatus"] as! Int  , deliver: aviso["emision"] as! String)
            self.lista.append(a)
            DispatchQueue.main.async {
                self.tabla.reloadData()
                
                
            }
        }
            DispatchQueue.main.async{
                if self.lista.count == 0 {
                
                    let alert = UIAlertController(title: "Avisos", message: "No has recibido ningún aviso", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: .destructive, handler: nil))
                    self.present(alert, animated: true)
                
            }
            }
            }
            self.removeSpinner()
    })
    
        
    }

    
    func guardaAvisos(id : String, title : String, body : String, status: Int, deliver : String) -> Aviso {
        let av = Aviso(dic: ["identificador" : id,
                                    "titulo" : title,
                                    "cuerpo" : body,
                                    "estatus" : status,
                                    "emision" : deliver
            
        ])
        
        return av
        
    }
    
    func creaAlerta (titulo:String , mensaje:String, mensajeBtn:String) {
    let alert = UIAlertController (title: titulo , message: mensaje, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: mensajeBtn, style: .default, handler: nil))
    self.present(alert , animated: true , completion: nil) }
    

}

extension Avisos: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "celdaAviso", for: indexPath) as! CeldaAvisos
        
        c.delegate = self
        
        c.txtTitulo.text = self.lista[indexPath.row].title
        c.txtFecha.text = self.lista[indexPath.row].deliver
        c.aviso = lista[indexPath.row]
//        print(lista[indexPath.row].status)
//        print("El id es \(lista[indexPath.row].id)")
        if lista[indexPath.row].status == 1  {
            DispatchQueue.main.async{
                c.viewStatus.backgroundColor = .clear }
        }else{
            DispatchQueue.main.async{
                c.viewStatus.backgroundColor = #colorLiteral(red: 0.8102815747, green: 0.1906022131, blue: 0.04933752865, alpha: 1)}
        }
        return c
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    
    
}

extension Avisos : Ver {
    func verAviso(aviso: Aviso) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "detalles") as! DetallesAvisos
        controller.aviso = aviso
        
        self.present(controller,animated: true,completion: nil)
    }
    
    
}
