//
//  ListaDeReportes.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 30/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import UIKit

class ListaDeReportes: UIViewController {
    
    
    let service = RegistroEmpleadoRequestTO()
    var lista: [ReportesList] = []
    var arraylist: [Dictionary<String,Any>]?
    
    @IBOutlet weak var tabla: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabla.delegate = self
        self.tabla.dataSource = self
        
       
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
//        if self.lista.count == 0 {
//                             DispatchQueue.main.async {
//                                 let alert = UIAlertController(title: "Reportes", message: "No tienes reportes registrados", preferredStyle: .alert)
//                                 alert.addAction(UIAlertAction(title: "Aceptar", style: .destructive, handler: nil))
//                                 self.present(alert, animated: true)
//                             }
//                         }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        obtieneReportes()
    }
    
    


func obtieneReportes(){
    self.showSpinner(onView: self.view)
    guard let numEmp = TabBarViewController.numEmpSes else {return}
    self.service.listaDeReportes(numEmpleado: numEmp, callback: { json,mensaje in
        
        if mensaje == "Error"{
            DispatchQueue.main.async{
                self.creaAlerta(titulo: "Advertencia", mensaje: "No se pudo conectar con el servidor, por favor intente mas tarde", mensajeBtn: "Aceptar")}
        }else{
        print(json)
        self.removeSpinner()
        self.arraylist = json["reportes"] as? [Dictionary<String,Any>]
            
        for rep in self.arraylist! {
            let r = self.guardaReporte(folio: rep["folio"] as! String , cuerpo: rep["cuerpo"] as! String , tipo: rep["clasificacion"] as! String, estatus: rep["estatus"] as! String)
            self.lista.append(r)
            DispatchQueue.main.async {
                self.tabla.reloadData()
            }
            
            
        }
            if self.arraylist?.count == 0 {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Reportes", message: "No tienes reportes registrados", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: .destructive, handler: nil))
                    self.present(alert, animated: true)
                }

            }

        }
    })
    
    
    }
    
    func guardaReporte(folio : String, cuerpo : String, tipo : String, estatus : String) -> ReportesList {
        let rp = ReportesList(dic: ["folio" : folio,
                                    "cuerpo" : cuerpo,
                                    "clasificacion" : tipo,
                                    "estatus" : estatus
            
        ])
        
        return rp
        
    }
    
    func creaAlerta (titulo:String , mensaje:String, mensajeBtn:String) {
    let alert = UIAlertController (title: titulo , message: mensaje, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: mensajeBtn, style: .default, handler: nil))
    self.present(alert , animated: true , completion: nil) }


}

    extension ListaDeReportes: UITableViewDelegate, UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
          
            return lista.count
        }
        

        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let c = tableView.dequeueReusableCell(withIdentifier: "custom", for: indexPath) as! Celda
            
            c.lblNumFolio.text = self.lista[indexPath.row].strFolio
            c.lblCuerpo.text = self.lista[indexPath.row].strCuerpo
            c.lblClasifiacion.text = self.lista[indexPath.row].strTipo
            c.lblStatus.text = self.lista[indexPath.row].strStatus
            
            
            c.tag = indexPath.row
                   
            
            return c
        }
        
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 125
        }
        
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        }
        
    }
   


