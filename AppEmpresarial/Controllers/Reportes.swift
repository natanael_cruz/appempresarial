//
//  Reportes.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 25/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import UIKit

class Reportes: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    let allowedCharacters = CharacterSet(charactersIn:"0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvxyzáéíóúÁÉÍÓÚ¿?!¡ ,.:;").inverted
    var placeholder:UILabel!
    @IBOutlet weak var viewCollection: UIView!
    let service : RegistroEmpleadoRequestTO = RegistroEmpleadoRequestTO()
    let verify = Validation()
    @IBOutlet weak var txtDescripcion: UITextView!
    @IBOutlet weak var btnEnviar: UIButton!
    var type:Int = 0
    @IBOutlet weak var lblTipo: UILabel!
    @IBOutlet weak var lblServicio: UILabel!
    @IBOutlet weak var lblMantenimiento: UILabel!
    @IBOutlet weak var lblTecnico: UILabel!
    @IBOutlet weak var lblAdministrativo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboard()
        
        
        txtDescripcion.textColor = UIColor.lightGray
        self.setUpTextView()
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        txtDescripcion.isHidden = true
        btnEnviar.isHidden = true
        lblTipo.isHidden = true
        txtDescripcion.text = ""
        
        viewCollection.layer.shadowColor = UIColor.black.cgColor
        viewCollection.layer.shadowOpacity = 1
        viewCollection.layer.shadowOffset = .zero
        viewCollection.layer.shadowRadius = 10
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if CheckInternet.Connection(){
            
        }else{
            self.creaAlerta(titulo: "Advertencia", mensaje: "El dispositivo no esta conectado a internet, compruebe su conexión e intente de nuevo", mensajeBtn: "Aceptar")
        }
    }
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let components = text.components(separatedBy: allowedCharacters)
        let filtered = components.joined(separator: "")
        
        let auxText = (textView.text as! NSString).replacingCharacters(in: range, with: text)
        
        let numChars = auxText.count
        
        if text == filtered && numChars <= 100{
            
            return true

        } else {
            
            return false
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.tag == 0 {
            placeholder.isHidden = true
            textView.tag = 1
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty == true {
            placeholder.isHidden = false
            textView.tag = 0
        }
    }
    
    
    @IBAction func tipoTecnico(_ sender: UIButton) {
        self.txtDescripcion.isHidden = false
        self.btnEnviar.isHidden = false
        self.lblTipo.text = "<<Problema Tecnico>>"
        self.lblTecnico.textColor = #colorLiteral(red: 0, green: 0.5904014707, blue: 0.8873565197, alpha: 1)
        self.lblAdministrativo.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lblServicio.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lblMantenimiento.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        self.lblTipo.isHidden = false
        sender.isHighlighted = true
        type = 1
        print (type)
        
        
    }
    
    @IBAction func tipoAdministrativo(_ sender: UIButton) {
        self.txtDescripcion.isHidden = false
        self.btnEnviar.isHidden = false
        sender.isHighlighted = true
        self.lblTipo.text = "<<Problema Administrativo>>"
        self.lblTecnico.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lblAdministrativo.textColor = #colorLiteral(red: 0, green: 0.5904014707, blue: 0.8873565197, alpha: 1)
        self.lblServicio.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lblMantenimiento.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        self.lblTipo.isHidden = false

        type = 3
        print(type)
    }
    
    @IBAction func tipoServicio(_ sender: UIButton) {
        self.txtDescripcion.isHidden = false
        self.btnEnviar.isHidden = false
        self.lblTipo.text = "<<Problema de Servicios>>"
        self.lblTecnico.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lblAdministrativo.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lblServicio.textColor = #colorLiteral(red: 0, green: 0.5904014707, blue: 0.8873565197, alpha: 1)
        self.lblMantenimiento.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        self.lblTipo.isHidden = false

        sender.isHighlighted = true
        type = 4
        print(type)
        
    }
    
    @IBAction func tipoMantenimiento(_ sender: UIButton) {
        self.txtDescripcion.isHidden = false
        self.btnEnviar.isHidden = false
        self.lblTipo.text = "<<Problema de Mantenimiento>>"
        self.lblTecnico.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lblAdministrativo.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lblServicio.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lblMantenimiento.textColor = #colorLiteral(red: 0, green: 0.5904014707, blue: 0.8873565197, alpha: 1)
//        self.lblTipo.isHidden = false
        
        sender.isHighlighted = true
        type = 2
        print(type)
        
    }
    
    
    @IBAction func enviarReporte(_ sender: Any) {
        if CheckInternet.Connection(){
        guard let cuerpo = txtDescripcion.text else {return}
        
        guard let numEmpleado = TabBarViewController.numEmpSes else {return}
        
        if cuerpo.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            generaReporte(cuerpo: cuerpo.trimmingCharacters(in: .whitespacesAndNewlines), numEmp: numEmpleado, type: type )
            
        }else{
            self.creaAlerta(titulo: "Error", mensaje: "No puede estar vacia la descripcion", mensajeBtn: "Aceptar")
        }
//        let alert = UIAlertController (title: "Prueba" , message: "\(type)", preferredStyle: .alert)
//               alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
//               self.present(alert , animated: true , completion: nil)
        }else{
            self.creaAlerta(titulo: "Advertencia", mensaje: "El dispositivo no esta conectado a internet, compruebe su conexión e intente de nuevo", mensajeBtn: "Aceptar")
        }
        
        
    }
    
    func generaReporte(cuerpo:String, numEmp : String , type : Int)
    {
        self.showSpinner(onView: self.view)
        let reporte = Reporte(dic: ["numeroDeEmpleado" : numEmp, "cuerpo" : cuerpo, "clasificacion" : type ])
        service.serviceReport(reporte: reporte, callback: {mensaje in
            self.removeSpinner()
            DispatchQueue.main.async{
            self.creaAlerta(titulo: "", mensaje: mensaje, mensajeBtn: "Aceptar")
                self.txtDescripcion.text = ""
            }
        })
        
    }
    
    func setUpTextView(){
        txtDescripcion.text = ""
        txtDescripcion.delegate = self
        txtDescripcion.layer.borderColor = UIColor.lightGray.cgColor //CGColor(srgbRed: 0, green: 0, blue: 0, alpha: 1)
        txtDescripcion.layer.borderWidth = 0.5
        txtDescripcion.layer.cornerRadius = 5.0
        txtDescripcion.clipsToBounds = true
        placeholder = UILabel(frame: CGRect(x: 6, y: 10, width: 250, height: 14))
        placeholder.textColor = UIColor.lightGray
        placeholder.font = UIFont.boldSystemFont(ofSize: 14.0)
        placeholder.textAlignment = NSTextAlignment.left
        placeholder.text = "Descripción del problema"
        txtDescripcion.addSubview(placeholder)
        txtDescripcion.textContainerInset = UIEdgeInsets(top: 8.5, left: 7, bottom: 4, right: 0)
    }
    
    
    func creaAlerta (titulo:String , mensaje:String, mensajeBtn:String) {
           let alert = UIAlertController (title: titulo , message: mensaje, preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: mensajeBtn, style: .default, handler: nil))
           self.present(alert , animated: true , completion: nil) }
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }

}


