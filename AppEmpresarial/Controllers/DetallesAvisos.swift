//
//  DetallesAvisos.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 08/01/20.
//  Copyright © 2020 Natanael Cruz Mota. All rights reserved.
//

import UIKit

class DetallesAvisos: UIViewController {
    
    var aviso: Aviso?
    var idAviso : String = ""
    let vistaAvisos : Avisos = Avisos()
    let service : ServiceAvisos = ServiceAvisos()
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtCuerpo: UITextView!
    @IBOutlet weak var btnEliminar: UIButton!
    @IBOutlet weak var contenedor: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        idAviso = aviso?.id ?? "no se pudo"
        print("El identificador del aviso seleccionado es: \(idAviso)")
      
      
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblTitle.text = aviso?.title ?? "No tiene nada"
        txtCuerpo.text = aviso?.body ?? "Cuerpo vacio"
        contenedor.layer.shadowColor = UIColor.black.cgColor
        contenedor.layer.shadowOpacity = 1
        contenedor.layer.shadowOffset = .zero
        contenedor.layer.shadowRadius = 10
        contenedor.layer.cornerRadius = 10
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let numemp = TabBarViewController.numEmpSes!
        self.showSpinner(onView: self.view)
        service.cambiarEstatus(numEmpleado: numemp, id: idAviso, callback: {mensaje in
            self.removeSpinner()
            print(mensaje)
            
        })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        service.cambiarEstatus(numEmpleado: TabBarViewController.numEmpSes!, id: idAviso, callback: { mensaje in
//            print(mensaje)
            
//        })
    }
    
    
    @IBAction func eliminarAviso(_ sender: UIButton) {
        self.showSpinner(onView: self.view)
        service.eliminaAviso(numEmpleado: TabBarViewController.numEmpSes!, id: idAviso, callback: { mensaje in
            self.removeSpinner()
            DispatchQueue.main.async{
            let alerta = UIAlertController(title:"", message: "El aviso se eliminara. ¿Estás de acuerdo?", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: {
                (action : UIAlertAction) in

                self.dismiss(animated: true, completion: nil)
                
            }))
            alerta.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alerta, animated: true, completion: nil)
            
            
        }
        })
        
        
        
        
    }
    
    @IBAction func Back(_ sender: Any) {

       
        self.dismiss(animated: true, completion: nil)
        
    }
    
//    func setGradientBackground() {
//        let colorTop = UIColor(red: 245/255.0, green: 247/255.0, blue: 2249/255.0, alpha: 1.0).cgColor
//        let colorBottom = UIColor(red: 216/255.0, green: 237/255.0, blue: 249/255.0, alpha: 1.0).cgColor
//
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = [colorTop, colorBottom]
//        gradientLayer.locations = [0.0, 1.0]
//        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.contenedor.frame.width, height: self.contenedor.frame.height)
//
//
//        self.contenedor.layer.insertSublayer(gradientLayer, at:0)
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
