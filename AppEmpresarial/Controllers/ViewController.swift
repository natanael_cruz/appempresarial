//
//  ViewController.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 17/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import UIKit
import CoreData
import LocalAuthentication

class ViewController: UIViewController {
    
    var strMensajeAlerta = String()
    let context = LAContext()
    var error : NSError?
    let registro = RegistroViewController()
    var listaEstados: [Dictionary<String, Any>] = []
    var manageObjectEstados:[NSManagedObject] = []
    var manageObject:NSManagedObject?
    var managedObjectBiometricos : NSManagedObject?
    @IBOutlet weak var navigationBar: UINavigationItem!
    let userdefault = UserDefaults.standard
    let service: RegistroEmpleadoRequestTO = RegistroEmpleadoRequestTO()
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtNumEmpleado: UITextField!
    @IBOutlet weak var titleView: UINavigationItem!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var swRecuerdame: UISwitch!
    @IBOutlet weak var btnAutentica: UIButton!
    
    
    var idEmp : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.actualizaCore()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.06666666667, green: 0.5725490196, blue: 0.8392156863, alpha: 1)
        
        
        
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        hideKeyboard()
        
        self.guardaEstados()
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let contextBiometricos = appDelegate!.persistentContainer.viewContext
        let requestBiometricos = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        requestBiometricos.returnsObjectsAsFaults = false
        print(">>>>>>>>>>>>>><\(manageObject)<<<<<<<<<<<<<<<<<")
        do {
            let result = try contextBiometricos.fetch(requestBiometricos)
            for data in
                result as! [NSManagedObject]{
                    managedObjectBiometricos = data
                    btnAutentica.isEnabled = true
                    
                    if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error){
                        switch context.biometryType {
                        case .faceID:
                            btnAutentica.setImage(UIImage(named: "faceID"), for: UIControl.State.normal)
                            self.strMensajeAlerta = "Desea identificarse con FaceID?"
                            break
                        case .touchID:
                            btnAutentica.setImage(UIImage(named: "finger"), for: UIControl.State.normal)
                            self.strMensajeAlerta = "Desea identificarse con TouchID?"
                            break
                        case .none:
                            print("No biometric sensor")
                            break
                        default:
                            print("Other")
                        }
                    }
                    else{
                        if let err = error {
                            let strMessager = self.errorMessage(errorCode: err._code)
                            self.notifyUser("Error", err: strMessager)
                        }
                    }
            }
        }catch {
            print("Failed")
        }
        
        //        Observadores
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification: )), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification: )), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification: )), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    deinit {
            //Stop listening for keyboard hide/show events
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if CheckInternet.Connection() {
            if self.managedObjectBiometricos != nil {
                if let strNumEmp = managedObjectBiometricos?.value(forKey: "numeroEmpleado") as? String {
                    self.txtNumEmpleado.text = strNumEmp
                }
            }
        }else{
            alertaInternet(msj: "El dispositivo no esta conectado a internet, compruebe su conexión e intente de nuevo")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.txtPassword.text = userdefault.string(forKey: "password")  ?? ""
                self.txtNumEmpleado.text = userdefault.string(forKey: "empleado")  ?? ""
                
                if (self.txtPassword.text != "" && self.txtNumEmpleado.text != ""){
                    self.swRecuerdame.isOn = true
        //            llamaLogin(numEmp: txtNumEmpleado.text!, pass: txtPassword.text!)
                }else { self.swRecuerdame.isOn = false}
        
    }

    @IBAction func Login(_ sender: UIButton) {
        if CheckInternet.Connection() {
        guard let strNumEmplado = txtNumEmpleado.text else {
            return
        }
        
        
        guard let strPass = txtPassword.text else {
            return
        }
        
        let auxPass = strPass.cifrar(input: strPass)
        
        
            if strNumEmplado.validaNumeroEmpleado(cadena: strNumEmplado) != true {
                alertaInternet(msj: "Numero de empleado invalido")
            }
            
            if (strNumEmplado == "" || strPass == "") {
                alertaInternet(msj: "Por favor ingresa Numero de Empleado y Password")
            }
            if (strNumEmplado.count < 6){
                alertaInternet(msj: "El numero de usuario debe tener 6 caracteres")
            }
            
            if((strNumEmplado.count == 6) && (strPass != "") ) {
                if swRecuerdame.isOn {
                    userdefault.set(strNumEmplado, forKey: "empleado")
                    userdefault.set(strPass, forKey: "password")
                    self.swRecuerdame.isOn = true
                }
                llamaLogin(numEmp: strNumEmplado, pass: auxPass)
                print(auxPass)
            }
            
        }else{
            
            alertaInternet(msj: "El dispositivo no esta conectado a internet, compruebe su conexión e intente de nuevo")
        }
        
    }
    
    func llamaLogin(numEmp : String, pass : String) {
        self.showSpinner(onView: self.view)
        service.Logeo(usr: numEmp, pass: pass, callback: { mensaje in
            print(mensaje)
            self.removeSpinner()
            if (mensaje == "0") {
                
                let alert = UIAlertController(title: "App Empresarial", message: "¿Deseas asociar este usuario para autenticar con FaceID y/o TouchID (Si ya tiene asociada un usuario, será reemplazado)", preferredStyle: .alert)
                let si = UIAlertAction(title: "Si", style: .default, handler: { (action : UIAlertAction) in
                    
                    self.goToComplete(num: numEmp)
                    guard let objdata = self.manageObject else {
                            self.guardaCoreData(numEmp: numEmp, password: pass)
                            return}
                    
                        objdata.setValue(numEmp, forKey: "numeroEmpleado")
                        objdata.setValue(pass, forKey: "password")
                        
                            
                    
                })
                
                let no = UIAlertAction(title: "No", style: .default, handler: { (action : UIAlertAction) in
                    
                    self.goToComplete(num: numEmp)
                    
                })
                alert.addAction(si)
                alert.addAction(no)
                self.present(alert, animated: true , completion: nil)
                
                
            }
            if (mensaje == "3"){
                
                    self.alertaInternet(msj: "Usuario y/o Password incorrectos")
                
            }
            
            if (mensaje != "0" || mensaje != "3"){
                self.alertaInternet(msj: mensaje)
            }
            
        })
        }
    
    func loginBiometrics(numEmp : String, pass : String) {
    self.showSpinner(onView: self.view)
    service.Logeo(usr: numEmp, pass: pass, callback: { mensaje in
        print(mensaje)
        self.removeSpinner()
        if (mensaje == "0") {
                
                self.goToComplete(num: numEmp)
            
            
        }
        if (mensaje == "3"){
            
                self.alertaInternet(msj: "Usuario y/o Password incorrectos")
            
        }
        
        if (mensaje != "0" || mensaje != "3"){
            self.alertaInternet(msj: mensaje)
        }
        
    })
    }
    
    func goToComplete(num : String) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        if let controller = story.instantiateViewController(identifier: "tabBar") as? TabBarViewController{
            TabBarViewController.numEmpSes = num
            self.present(controller, animated: true, completion: nil)}
    }
    
    @IBAction func seePass(_ sender: UIButton) {
        if (txtPassword.isSecureTextEntry){
            sender.setImage(UIImage(named: "HideIcon"), for: .normal)
            txtPassword.isSecureTextEntry = false
            sender.tintColor = UIColor(displayP3Red: 41/255, green: 162/255, blue: 1, alpha: 1)
        }else{
            sender.setImage(UIImage(named: "SeeIcon"), for: .highlighted)
            txtPassword.isSecureTextEntry = true
            sender.tintColor = .gray
        }
    }
    
    
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func alertaInternet (msj : String){
        let alert = UIAlertController(title: "Atencion", message: msj, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func guardaCoreData(numEmp : String, password: String){
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Usuario", in: managedContext)!
        let managedObject = NSManagedObject(entity: entity, insertInto: managedContext)
        
        
        managedObject.setValue(numEmp, forKeyPath: "numeroEmpleado")
        managedObject.setValue(password, forKeyPath: "password")

        do{
            try managedContext.save()
           
        }catch let error as NSError {
            print("\(error.userInfo)")
        }
        
        actualizaCore()
        
    }
    
    func actualizaCore(){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        request.returnsObjectsAsFaults = false
        
       do{
         let result = try managedContext.fetch(request)
        for user in result as! [NSManagedObject] {
            self.manageObject = user
        }
              }catch let error as NSError {
                  print("\(error.userInfo)")
              }
        
    }
    
//    Inicar Sesion con Biometricos
    
    @IBAction func identificar(_ sender: Any) {
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error){
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                                   localizedReason: self.strMensajeAlerta,
                                   reply: { [unowned self] (success, error) -> Void in
                                    
                                    DispatchQueue.main.async {
                                        if (success) {
                                            print("Identificado correctamente")
                                            guard let strnum = self.managedObjectBiometricos?.value(forKey: "numeroEmpleado") else {return}
                                            guard let strpass = self.managedObjectBiometricos?.value(forKey: "password") else {return}
                                            
                                            self.loginBiometrics(numEmp: strnum as! String, pass: strpass as! String)
                                        }else{
                                            if let error = error {
                                                let strMessage = self.errorMessage(errorCode: error._code)
                                                self.notifyUser("Error", err: strMessage)
                                            }
                                        }
                                    }
            } )
        }
    }
    func guardaEstados(){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
              let managedContext = appDelegate!.persistentContainer.viewContext
              let fetchreques = NSFetchRequest<NSManagedObject>(entityName: "Estados")
        let entity = NSEntityDescription.entity(forEntityName: "Estados", in: managedContext)
        let cantidad = try! managedContext.count(for: fetchreques)
        
        if cantidad > 0 {
            print("ya hay informacions en coredata, no necesitamos datos por default")
            print("Hay \(cantidad) elementos guardados")
        }else{
            print("Se agregaron datos por defecto")
            let archivo = Bundle.main.path(forResource: "EstadosPersistentes", ofType: "plist")
            self.listaEstados = NSArray(contentsOfFile: archivo!)! as! [Dictionary<String, Any>]
            
            for state in listaEstados{
                let managedObject = NSManagedObject(entity: entity! , insertInto: managedContext)
                managedObject.setValue(state["ent"], forKey: "nombre")
                
                
            }
            
            do {
                try managedContext.save()
            } catch let error as NSError {
                print(error.userInfo)
            }
        }
        
    }
    
    
    func errorMessage(errorCode:Int) -> String {
       var strMessage = ""
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            strMessage = "Authentication Failed"
            break
        case LAError.userCancel.rawValue:
            strMessage = "User Cancel"
            break
        case LAError.userFallback.rawValue:
            strMessage = "User FallBack"
            break
        case LAError.systemCancel.rawValue:
            strMessage = "System Cancel"
            break
        case LAError.passcodeNotSet.rawValue:
            strMessage = "Passcode not set"
            break
        case LAError.biometryNotAvailable.rawValue:
            strMessage = "TouchID Not Available"
            break
        case LAError.appCancel.rawValue:
            strMessage = "App Cancel"
            break
        case LAError.biometryLockout.rawValue:
            strMessage = "Lockout"
        default:
            strMessage = "Some Error Found"
            break
        }
        return strMessage
    }
    
    func notifyUser(_ msg:String, err:String?){
        let alert = UIAlertController(title: msg, message: err, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func keyboardWillChange(notification: Notification){
        
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        view.frame.origin.y = -keyboardRect.height + 100
        
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
            
        }else{
            view.frame.origin.y = 0
        }
        
    }
    
}

