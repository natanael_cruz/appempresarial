//
//  InicioViewController.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 02/01/20.
//  Copyright © 2020 Natanael Cruz Mota. All rights reserved.
//

import UIKit

class InicioViewController: UIViewController {
    let userdefault = UserDefaults.standard
    @IBOutlet weak var tabla: UITableView!
    var listaAsistencia: [Asistencia]  = []
    var arraylist: [Dictionary<String,Any>]?
    @IBOutlet weak var lblDatos: UILabel!
    let service = ServiceEmpleado()
    @IBOutlet weak var viewDatos: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewDatos.layer.shadowColor = UIColor.black.cgColor
        viewDatos.layer.shadowOpacity = 1
        viewDatos.layer.shadowOffset = .zero
        viewDatos.layer.shadowRadius = 10
        setGradientBackground()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if CheckInternet.Connection(){
            
        }else{
            self.alerta(mensaje: "El dispositivo no esta conectado a internet, compruebe su conexión e intente de nuevo")
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let num = TabBarViewController.numEmpSes else {return}
        listaAsistencia = []
        obtieneAsistencia()
        service.getDatos(numEmpleado: num , callback: { mensaje, contador in
            
            DispatchQueue.main.async{
            self.lblDatos.text = mensaje
                
                print("Tienes \(contador) Pendientes")
//                if contador > 0{
                    self.alerta(mensaje: "Tienes \(contador) avisos sin leer") //}
            }
        })
        
        service.getValidaAsistencia(numEmpleado: num, callback: { mensaje, flag in
            TabBarViewController.registrado = flag
            
        })
        
    }

    @IBAction func LogOut(_ sender: Any) {
        let alert = UIAlertController(title: "Sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action : UIAlertAction) in
            
            DispatchQueue.main.async{
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let controller = story.instantiateViewController(identifier: "principal")
                        self.userdefault.removeObject(forKey: "empleado")
                        self.userdefault.removeObject(forKey: "password")
            //        self.present(controller, animated: true, completion: nil)
                self.dismiss(animated: true, completion: nil)
                        
                    
                    }
            
        }))
            
        self.present(alert,animated: true)
        
        
    }
    
    func obtieneAsistencia(){
        
        guard let numemp = TabBarViewController.numEmpSes else {return}
        self.service.listaDeAsistencias(numEmpleado: numemp, callback: { json in
            
            self.arraylist = json["asistencias"] as? [Dictionary<String, Any>]
            
            for asistencia in self.arraylist!{
                let a = self.guardaAsistencia(folio: asistencia["folio"] as! String, fecha: asistencia["fecha"] as! String)
                self.listaAsistencia.append(a)
                DispatchQueue.main.async {
                    self.tabla.reloadData()
                    
                }
                
            }
            
            
        })
        
    }
    
    
    
    func guardaAsistencia(folio : String, fecha : String) -> Asistencia {
        let asis = Asistencia(dic: ["folio" : folio,
                                    "fecha" : fecha
               
           ])
           
           return asis
           
       }
    
    func alerta(mensaje : String){
        let alerta = UIAlertController(title: "Advertencia", message: mensaje, preferredStyle: .alert)
        alerta.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alerta,animated: true, completion: nil)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 192.0/255.0, green: 242.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0).cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = CGRect(x: 0, y: 0, width: viewDatos.frame.width, height: viewDatos.frame.height)
        
        
        viewDatos.layer.insertSublayer(gradientLayer, at:0)
    }

}

extension InicioViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listaAsistencia.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "celdaAsistencia", for: indexPath) as! CeldaAsistencia
        
        let ff = self.listaAsistencia[indexPath.row].strHorario.prefix(10)
        let hora = self.listaAsistencia[indexPath.row].strHorario.suffix(8)
        c.lblAsistencia.text = "\(ff)"
        c.lblHora.text = "\(hora)"
        
        return c
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
}
