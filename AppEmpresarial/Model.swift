//
//  Model.swift
//  AppEmpresarial
//
//  Created by Natanael Cruz Mota on 20/12/19.
//  Copyright © 2019 Natanael Cruz Mota. All rights reserved.
//

import Foundation
import CryptoKit
import UIKit
import CoreData


struct EstadosRepublica {
    let strname : String
    
    init(dic : [String:Any]){
        self.strname = dic["nombre"] as? String ?? ""
    }
}

struct EmpleadoCore {
    let strNumero : String
    let strPassword : String
    
    init(dic : [String : Any]) {
        self.strNumero = dic["numeroEmpleado"] as? String ?? ""
        self.strPassword = dic["password"] as? String ?? ""
    }
}

struct Empleado: Decodable {
    let strName : String
    let strApPat : String
    let strApMat : String
    let strFecha : String
    let strEntFed : String
    let strPass : String
    let intEdad : Int
    
    init (dictionary: [String: Any]){
        
        

        
        
        self.strName = dictionary["nombres"] as? String ?? ""
        self.strApPat = dictionary["apellidoPaterno"] as? String ?? ""
        self.strApMat = dictionary["apellidoMaterno"] as? String ?? ""
        self.intEdad = dictionary["edad"] as? Int ?? 0
        self.strFecha = dictionary["fechaDeNacimiento"] as? String ?? ""
        self.strPass = dictionary["password"] as? String ?? ""
        self.strEntFed = dictionary["entidadFederativa"] as? String ?? ""
        
        
    }
    
    func request() -> [String:Any] {
        return [
            "nombres" : self.strName,
            "apellidoPaterno" : self.strApPat,
            "apellidoMaterno" : self.strApMat,
            "edad" : self.intEdad,
            "fechaDeNacimiento" : self.strFecha,
            "entidadFederativa" : self.strEntFed,
            "password" : self.strPass
        ]
    }
}

class Reporte: Decodable {
    
    let strNoEmpleado : String
    let strDescripcion : String
    let intTipo : Int
    
    init(dic: [String:Any]) {
        
        self.strNoEmpleado = dic["numeroDeEmpleado"] as? String ?? ""
        self.intTipo = dic["clasificacion"] as? Int ?? 0
        self.strDescripcion = dic["cuerpo"] as? String ?? ""
    
    }
    
    
}


class Asistencia: Decodable {
    let strHorario : String
    let strFolio : String
    
    init(dic : [String:Any]) {
        self.strFolio = dic["folio"] as? String ?? ""
        self.strHorario = dic["fecha"] as? String ?? ""
    }
}

class ReportesList {
    
    let strFolio : String
    let strCuerpo : String
    let strTipo : String
    let strStatus : String
    
    init(dic: [String:Any]) {
        
        self.strFolio = dic["folio"] as? String ?? ""
        self.strTipo = dic["clasificacion"] as? String ?? ""
        self.strCuerpo = dic["cuerpo"] as? String ?? ""
        self.strStatus = dic["estatus"] as? String ?? ""
    
    }
    
    
}

class Aviso {
    let id : String
    let title : String
    let body : String
    let status : Int
    let deliver : String
    
    init(dic: [String:Any]) {
        
        self.id = dic["identificador"] as? String ?? ""
        self.title = dic["titulo"] as? String ?? ""
        self.body = dic["cuerpo"] as? String ?? ""
        self.status = dic["estatus"] as? Int ?? 0
        self.deliver = dic["emision"] as? String ?? ""
    }
    
}

protocol RegistrableCell: class {
    static var nibName : String {get}
    static var reuseId : String {get}
    
}

extension RegistrableCell where Self: UITableViewCell{
    
    static func registrar(tabla:UITableView){
        let nibCell = UINib(nibName: nibName, bundle: nil)
        tabla.register(nibCell, forCellReuseIdentifier: reuseId)
    }
    
}


